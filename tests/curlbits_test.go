//###############################################################
// Copyright (C) 2020 James Fuller <jim.fuller@webcomposite.com>
// SPDX-License-Identifier: MIT
//###############################################################

// +build unit

package main

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestApp(t *testing.T) {
	assert.Equal(t, 1, 1)
}
