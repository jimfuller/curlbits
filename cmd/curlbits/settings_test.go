//###############################################################
// Copyright (C) 2020 James Fuller <jim.fuller@webcomposite.com>
// SPDX-License-Identifier: MIT
//###############################################################

// +build unit

package main

import (
	"testing"

	"fyne.io/fyne/app"
	"fyne.io/fyne/theme"
	"github.com/stretchr/testify/assert"
)

// test logging is inited properly
func TestThemeToggle(t *testing.T) {
	ui := &mainUI{
		app: app.New(),
	}
	toggleTheme(ui)
	assert.Equal(t, ui.app.Settings().Theme(), theme.LightTheme())
}
