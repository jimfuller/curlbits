//###############################################################
// Copyright (C) 2020 James Fuller <jim.fuller@webcomposite.com>
// SPDX-License-Identifier: MIT
//###############################################################

// entry point for curlbits.
package main

import (
	"os"

	"fyne.io/fyne/app"
	log "github.com/sirupsen/logrus"
)

func init() {
	if os.Getenv("DEBUG") == "true" {
		log.SetFormatter(&log.TextFormatter{})
		log.SetLevel(log.DebugLevel)
		log.SetReportCaller(true)
	} else {
		log.SetFormatter(&log.TextFormatter{})
		log.SetLevel(log.InfoLevel)
	}
}

func main() {
	log.Info("initiating curlbits app")
	curlbitsApp := app.New()

	curlbitsApp.SetIcon(getAppIcon())

	ui := initMainUI(curlbitsApp)
	mainWindow := ui.loadUI()

	ui.log.SetText("starting curlbits")

	wireAllLogic(ui)
	ui.uri.FocusGained()
	mainWindow.ShowAndRun()
}
