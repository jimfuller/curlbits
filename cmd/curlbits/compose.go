//###############################################################
// Copyright (C) 2020 James Fuller <jim.fuller@webcomposite.com>
// SPDX-License-Identifier: MIT
//###############################################################

// gui builders
package main

import (
	"net/url"

	"fyne.io/fyne"
	"fyne.io/fyne/layout"
	"fyne.io/fyne/theme"
	"fyne.io/fyne/widget"
	log "github.com/sirupsen/logrus"
)

type mainUI struct {
	app               fyne.App
	protocol          *widget.Select
	uri               *widget.Entry
	link              *widget.Hyperlink
	log               *widget.Entry
	copyAsCurl        *widget.Entry
	copyAsCurlArr     []string
	requestHeaders    *widget.Entry
	requestBody       *widget.Entry
	requestAccordion  *widget.Accordion
	responseHeaders   *widget.Entry
	responseBody      *widget.Entry
	responseAccordion *widget.Accordion
	options           *widget.ScrollContainer
	optionTabs        *widget.TabContainer
	mainScreen        *fyne.Container
	executeButton     *widget.Button
	mainAccordion     *widget.Accordion
	input             *widget.ScrollContainer
	optionsFlags	  map[string]bool
}

func makeRequestInput(m *mainUI) *widget.ScrollContainer {
	container := widget.NewHScrollContainer(widget.NewMultiLineEntry())
	container.SetMinSize(fyne.NewSize(default_width,50))
	m.input = container
	return container
}

func makeAddressBar(m *mainUI) *fyne.Container {
	//combo := widget.NewSelect([]string{"HTTP", "HTTPS", "FTP"}, func(value string) {})
	//m.protocol = combo
	urlEntry := widget.NewEntry()

	m.uri = urlEntry
	urlEntry.SetText("https://httpbin.org/get")
	urlEntryScoll := widget.NewHScrollContainer(urlEntry)
	urlEntryScoll.SetMinSize(fyne.NewSize(750,20))

	urlButton := widget.NewButton("", func() {
	})
	m.executeButton = urlButton
	urlButton.SetIcon(theme.ContentRedoIcon())
	urlBar := widget.NewHSplitContainer(urlEntryScoll, urlButton)
	urlBar.SetOffset(1)
	u, err := url.Parse(m.uri.Text)
	if err != nil {
		log.Fatal(err)
	}
	hyperlinkButton := widget.NewHyperlinkWithStyle(m.uri.Text, u, fyne.TextAlignCenter, fyne.TextStyle{Bold: true})
	m.link = hyperlinkButton
	container := fyne.NewContainerWithLayout(layout.NewVBoxLayout(),
		urlBar, hyperlinkButton)
	return container
}

func makeHTTPOptionsTab(m *mainUI) *widget.TabItem{
	form := widget.NewForm(
		widget.NewFormItem("",
			widget.NewCheck("verbose",func(b bool){
				m.optionsFlags["verbose"]=true
				m.copyAsCurlArr=append(m.copyAsCurlArr, "-V")
				updateCurlCommand(m)
			}),
		),
	)
	tabItem := widget.NewTabItem("http",form)
	return tabItem
}

func makeOptionsTabs(m *mainUI) *widget.TabContainer {
	container := widget.NewTabContainer()
	container.Append(
		makeHTTPOptionsTab(m),
	)
	//for i := 0; i < len(categories); i++ {
	//	container.Append(
	//		widget.NewTabItem(categories[i], widget.NewForm(
	//			widget.NewFormItem("",widget.NewCheck("verbose",func(b bool){})),
	//			)),
	//	)
	//}
	m.optionTabs=container
	return container
}

func makeOptionsContent(m *mainUI) *widget.ScrollContainer {
	container := widget.NewHScrollContainer(makeOptionsTabs(m))
	container.SetMinSize(fyne.NewSize(default_width,150))
	m.options = container
	return container
}

func makeResponseHeader(m *mainUI) *widget.ScrollContainer {
	responseHeaders := widget.NewMultiLineEntry()
	responseHeadersScrolling := widget.NewScrollContainer(responseHeaders)
	responseHeadersScrolling.SetMinSize(fyne.NewSize(0, 200))
	m.responseHeaders = responseHeaders
	return responseHeadersScrolling
}

func makeResponseBody(m *mainUI) *widget.ScrollContainer {
	responseBody := widget.NewMultiLineEntry()
	responseBodyScrolling := widget.NewScrollContainer(responseBody)
	responseBodyScrolling.SetMinSize(fyne.NewSize(0, 200))
	m.responseBody = responseBody
	return responseBodyScrolling
}

func makeVerboseLog(m *mainUI) *widget.ScrollContainer {
	verboseLog := widget.NewMultiLineEntry()
	m.log = verboseLog
	verboseListing := widget.NewVScrollContainer(
		verboseLog,
	)
	verboseListing.SetMinSize(fyne.NewSize(default_width, 10))
	return verboseListing
}

func makeCurlCommandListing(m *mainUI) *widget.ScrollContainer {
	curlCommandEntry := widget.NewMultiLineEntry()
	m.copyAsCurl = curlCommandEntry
	curlCommandEntry.SetText("curl -X POST  --anyauth --user admin:admin\n" +
		" --header \"Content-Type:application/json\" \n" +
		"-d@etc/test_trigger.json\n " +
		"http://localhost:8002/manage/v2/databases/kerndaten-triggers/triggers")
	curlCommandListing := widget.NewVScrollContainer(
		curlCommandEntry,
	)
	curlCommandListing.SetMinSize(fyne.NewSize(default_width, 10))
	return curlCommandListing
}

func makeMainAccordion(m *mainUI) *widget.Accordion {
	mainAccordion := widget.NewAccordion(
		widget.NewAccordionItem("request input",
			makeRequestInput(m)),
		widget.NewAccordionItem("response headers",
			makeResponseHeader(m)),
		widget.NewAccordionItem("response body",
			makeResponseBody(m)),
		widget.NewAccordionItem("options",
			makeOptionsTabs(m)),
		widget.NewAccordionItem("curl command",
			makeCurlCommandListing(m)),
		widget.NewAccordionItem("verbose log",
			makeVerboseLog(m)),
	)
	mainAccordion.MultiOpen=true
	m.mainAccordion = mainAccordion
	return mainAccordion
}

func makeMainContent(m *mainUI) *fyne.Container {
	container := fyne.NewContainerWithLayout(layout.NewVBoxLayout(),
		makeAddressBar(m), makeMainAccordion(m))
	return container
}

func (m *mainUI) loadUI() fyne.Window {
	mainApp := m.app.NewWindow(title)

	content := makeMainContent(m)
	m.mainScreen = content
	mainApp.SetContent(content)
	mainApp.Resize(fyne.NewSize(default_width, default_height))
	toggleTheme(m)
	return mainApp
}

func initMainUI(curlbitsApp fyne.App) *mainUI {
	log.Debug("initialising main ui")
	ui := &mainUI{
		app: curlbitsApp,
		optionsFlags: map[string]bool{},
		copyAsCurlArr: []string{"curl"},
	}
	return ui
}
