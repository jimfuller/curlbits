//###############################################################
// Copyright (C) 2020 James Fuller <jim.fuller@webcomposite.com>
// SPDX-License-Identifier: MIT
//###############################################################

// app settings logic.
package main

import (
	"bufio"
	"fyne.io/fyne"
	"fyne.io/fyne/theme"
	log "github.com/sirupsen/logrus"
	"io/ioutil"
	"os"
)

var title= "curlbits"
var default_width = 800
var default_height = 600

var categories = [...]string{"auth", "connection","curl", "dns", "file", "ftp",
"http", "imap", "misc", "output", "pop3", "post", "proxy", "scp", "sftp",
"smtp", "ssh", "telnet", "tftp", "tls", "upload", "verbose",
}

var tmpcategories = [...]string{"ftp", "http", "imap", "misc", "output",
	"pop3", "post", "proxy", "scp", "sftp", "smtp", "ssh", "telnet",
	"tftp", "tls", "upload", "verbose",
}

var options map[string]int

func getAppIcon() *fyne.StaticResource {
	iconFile, err := os.Open("./icon.png")
	if err != nil {
		log.Fatal(err)
	}
	r := bufio.NewReader(iconFile)
	b, err := ioutil.ReadAll(r)
	if err != nil {
		log.Fatal(err)
	}
	return fyne.NewStaticResource("icon", b)
}

func toggleTheme(m *mainUI) {
	if m.app.Settings().Theme() == theme.LightTheme() {
		log.Debug("setting dark theme")
		m.app.Settings().SetTheme(theme.DarkTheme())
	} else {
		log.Debug("setting light theme")
		m.app.Settings().SetTheme(theme.LightTheme())
	}
}
