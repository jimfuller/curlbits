//###############################################################
// Copyright (C) 2020 James Fuller <jim.fuller@webcomposite.com>
// SPDX-License-Identifier: MIT
//###############################################################

// gui builders
package main

import (
	"fmt"
	"strings"

	"github.com/andelf/go-curl"
)

func getHTTP(m *mainUI) string {

	var url = m.uri.Text
	easy := curl.EasyInit()
	defer easy.Cleanup()

	easy.Setopt(curl.OPT_URL, url)
	easy.Setopt(curl.OPT_VERBOSE, true)

	m.responseHeaders.SetText("")
	headerCallback := func(buf []byte, userdata interface{}) bool {
		if len(buf) > 0{
			m.mainAccordion.Open(1)
			m.responseHeaders.SetText(m.responseHeaders.Text + string(buf))
		}
		return true
	}

	easy.Setopt(curl.OPT_HEADERFUNCTION, headerCallback)

	var httpResult string
	// make a callback function
	fooTest := func(buf []byte, userdata interface{}) bool {
		httpResult += string(buf)
		return true
	}

	easy.Setopt(curl.OPT_WRITEFUNCTION, fooTest)

	if err := easy.Perform(); err != nil {
		fmt.Printf("ERROR: %v\n", err)
	}

	return httpResult

	//res, err := http.Get(url)
	//
	//if err != nil {
	//	panic(err.Error())
	//}
	//
	//body, err := ioutil.ReadAll(res.Body)
	//
	//if err != nil {
	//	panic(err.Error())
	//}
	//return string(body)
}

func openProtocolTab(m *mainUI,tabIndex int){
	m.mainAccordion.Open(3)
	m.optionTabs.SelectTabIndex(tabIndex)
}

func setProtocol(m *mainUI, s string) {
	if strings.HasPrefix(strings.ToLower(s), "http") {
		openProtocolTab(m,6)
	}
	if strings.HasPrefix(strings.ToLower(s), "https") {
		openProtocolTab(m,6)
	}
	if strings.HasPrefix(strings.ToLower(s), "ftp") {
		openProtocolTab(m,5)
	}
}

func wireAllLogic(m *mainUI) {

	//m.protocol.OnChanged = func(s string) {
	//	setProtocol(m, s)
	//	log.Debug("changed protocol selection to " + s)
	//}
	setProtocol(m,m.uri.Text)
	m.uri.OnChanged = func(s string) {
		if strings.HasPrefix(strings.ToLower(s), "http") {
			//m.protocol.SetSelected("HTTP")
		}
		if strings.HasPrefix(strings.ToLower(s), "https") {
			//m.protocol.SetSelected("HTTPS")
		}
		if strings.HasPrefix(strings.ToLower(s), "ftp") {
			//m.protocol.SetSelected("FTP")
		}
		m.link.SetURLFromString(m.uri.Text)
		m.link.SetText(m.uri.Text)

		updateCurlCommand(m)
		setProtocol(m, s)
	}

	m.executeButton.OnTapped = func() {
		m.uri.SetText(m.uri.Text)
		setProtocol(m,m.uri.Text)
		m.log.SetText(m.log.Text + "\nload url:" + m.uri.Text)
		result := getHTTP(m)
		if len(result) > 0{
			m.mainAccordion.Open(2)
			m.responseBody.SetText(result)
		}

		m.log.SetText("update log")
	}

}


func updateCurlCommand(m *mainUI){
	m.copyAsCurl.SetText(strings.Join(m.copyAsCurlArr," ") +" "+m.uri.Text)
}