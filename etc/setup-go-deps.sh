#!/bin/bash
go get -u golang.org/x/lint/golint
go get -u golang.org/x/tools/cmd/goimports
go get -u honnef.co/go/tools/cmd/staticcheck
go get -u golang.org/x/tools/cmd/godoc
go get -u fyne.io/fyne/cmd/fyne
