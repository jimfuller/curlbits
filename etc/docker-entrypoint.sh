#!/bin/sh


preload() {
  # generate fresh rsa key if needed
  [ ! -f "/etc/ssh/ssh_host_rsa_key" ] && ssh-keygen -f /etc/ssh/ssh_host_rsa_key -N '' -t rsa

  # generate fresh dsa key if needed
  [ ! -f "/etc/ssh/ssh_host_dsa_key" ] && ssh-keygen -f /etc/ssh/ssh_host_dsa_key -N '' -t dsa

  # set custom ssh_config
  envsubst <etc/ssh/ssh_config.tpl >/etc/ssh/ssh_config

  # prepare ssh run dir
  mkdir -p /var/run/sshd

  # set bashrc
  echo "PS1='\u[\W]# '" >>/root/.bashrc
  echo "PS1='\u[\W]\$ '" >>/home/${USER}/.bashrc

  # generate machine-id
  uuidgen > /etc/machine-id

  # set keyboard for all sh users
  echo "export QT_XKB_CONFIG_ROOT=/usr/share/X11/locale" >> /etc/profile
  source /etc/profile
  >/run/preloaded.lock;
}

[ ! -f /run/preloaded.lock ] && preload

exec "$@"