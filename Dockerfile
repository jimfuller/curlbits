FROM golang:1.14-alpine

ENV DISPLAY :99
ENV RESOLUTION 1920x1080x24

RUN mkdir /curlbits
ADD etc /etc

RUN apk --update --no-cache add \
	git zsh ca-certificates openssl build-base curl curl-dev libcurl supervisor

RUN echo "http://dl-cdn.alpinelinux.org/alpine/edge/testing" >> /etc/apk/repositories \
    && apk add --no-cache  sudo xvfb x11vnc xfce4 xfce4-xkb-plugin xf86-input-mouse xf86-input-keyboard faenza-icon-theme bash xfce4-terminal xf86-input-synaptics \
    && adduser -h /home/alpine -s /bin/bash -S -D alpine && echo -e "alpine\nalpine" | passwd alpine \
    && echo 'alpine ALL=(ALL) NOPASSWD:ALL' >> /etc/sudoers

RUN apk --update --no-cache add \
    libxcursor-dev libx11-dev libxrandr-dev libxinerama-dev libxi-dev linux-headers mesa-dev mesa-dri-gallium xvfb-run

RUN apk --update --no-cache add \
    virtualgl --update-cache --repository http://dl-3.alpinelinux.org/alpine/edge/testing/ --allow-untrusted

RUN /etc/setup-go-deps.sh

WORKDIR /home/alpine
EXPOSE 22 5900 5901 8000
USER alpine
RUN mkdir -p /home/alpine/.vnc && x11vnc -storepasswd alpine /home/alpine/.vnc/passwd

CMD ["/etc/entry.sh"]