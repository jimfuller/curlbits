module jfuller/curlbits

go 1.15

require (
	fyne.io/fyne v1.3.4-0.20200918231643-0a2fa00d4f28
	github.com/andelf/go-curl v0.0.0-20200630032108-fd49ff24ed97
	github.com/sirupsen/logrus v1.6.0
	github.com/stretchr/testify v1.6.1
	golang.org/x/sys v0.0.0-20200917073148-efd3b9a0ff20 // indirect
	gopkg.in/yaml.v3 v3.0.0-20200615113413-eeeca48fe776 // indirect
	honnef.co/go/tools v0.0.1-2020.1.5 // indirect
)
