export PATH := /Users/jimfuller/go/bin:$(PATH)

##################
# define defaults
##################
docker=`which docker`
dockercompose=`which docker-compose`
go=`which go`

##################
# local targets
##################
build:
	go build cmd/curlbits/curlbits.go cmd/curlbits/logic.go cmd/curlbits/compose.go cmd/curlbits/settings.go

run:
	./curlbits

buildrun: build run

test:
	go test -tags unit -coverprofile=cover.out -v ./...

format:
	go run golang.org/x/tools/cmd/goimports -v -w .

lint:
	go run honnef.co/go/tools/cmd/staticcheck -- $$(go list ./... | grep -v tempfork)

env:
	env

check: format lint test

docs:
	godoc -http=:8000

clean:
	rm -Rf ./curlbits
	rm -Rf *.tar.gz
	rm -Rf *.log
	rm -Rf reports

package: build clean
	fyne package -executable ./curlbits

setup-deps:
	etc/setup-go-deps.sh

##################
# docker targets
##################
docker-build:
	${docker} exec godev make build

docker-run:
	${docker} exec godev make run

docker-buildrun:
	${docker} exec godev make build run

docker-check:
	${docker} exec godev make check

docker-lint:
	${docker} exec godev make lint

docker-format:
	${docker} exec godev make format

docker-test:
	${docker} exec godev make test

docker-docs:
	${docker} exec godev make docs

docker-package:
	${docker} exec godev make package

####################
# docker env targets
####################
start-docker:
	${dockercompose} up -d --remove-orphans || true

stop-docker:
	${dockercompose} down --remove-orphans || true

build-docker:
	docker build --squash --compress -t "jfuller/godev" .